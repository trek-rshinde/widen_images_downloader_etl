package com.trek.etl.test;

import com.trek.etl.image.publisher.TrekImagePublisher;
import com.trek.etl.image.publisher.TrekImagePublisherDriver;
import com.trek.etl.image.service.WidenImageHarvestServiceImpl;
import com.trek.pojo.widen.AssetBatch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mdenomie on 11/15/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/tbcImagePublisherContext-Service.xml" })
public class TrekImagePublisherTest {

    @Resource
    private WidenImageHarvestServiceImpl widenImageHarvestServiceImpl;

    @Test
    public void noDateInputTest() throws Exception {
        Date date = new Date();
        AssetBatch assetBatch = widenImageHarvestServiceImpl.getInitialAssetBatch(date, date);
        Assert.assertNotNull(assetBatch);
        Assert.assertNotNull(assetBatch.getItems());
    }

    @Test
    public void dateRangeInputTest() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date startDate = formatter.parse("01/01/2017");
        Date endDate = formatter.parse("01/01/2018");

        AssetBatch assetBatch = widenImageHarvestServiceImpl.getInitialAssetBatch(startDate, endDate);
        Assert.assertNotNull(assetBatch);
        Assert.assertNotNull(assetBatch.getItems());
    }

    @Test
    public void scrollSearchTest() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date startDate = formatter.parse("01/01/2017");
        Date endDate = formatter.parse("01/01/2018");

        AssetBatch assetBatch = widenImageHarvestServiceImpl.getInitialAssetBatch(startDate, endDate);
        Assert.assertNotNull(assetBatch);
        Assert.assertNotNull(assetBatch.getItems());

        String scrollId = assetBatch.getScrollId();

        Assert.assertNotNull(scrollId);

        assetBatch = widenImageHarvestServiceImpl.getNextAssetBatch(scrollId);
        Assert.assertNotNull(assetBatch);
        Assert.assertNotNull(assetBatch.getItems());
    }
}
