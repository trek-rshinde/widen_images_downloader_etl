package com.trek.etl.image.publisher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mdenomie on 11/8/17.
 */
public class TrekImagePublisherDriver {
    private static Log logger = LogFactory.getLog(TrekImagePublisherDriver.class);

    public static void main(String[] args) {
        logger.info("Starting widen_to_scene7");
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "tbcImagePublisherContext-Service.xml");

        Date dateToPublish = new Date();
        Date endDateToPublish = new Date();

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            if (args.length == 1) {
                dateToPublish = formatter.parse(args[0]);
                endDateToPublish = formatter.parse(args[0]);
            } else if(args.length == 2){
                dateToPublish = formatter.parse(args[0]);
                endDateToPublish = formatter.parse(args[1]);
            }
        } catch(ParseException e){
            logger.error("Error parsing date parameter", e);
            return;
        }

        TrekImagePublisher trekImagePublisher = (TrekImagePublisher)context.getBean("trekImagePublisher");
        trekImagePublisher.run(dateToPublish, endDateToPublish);
    }


}
