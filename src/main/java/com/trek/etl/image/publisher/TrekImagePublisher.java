package com.trek.etl.image.publisher;

import com.trek.etl.image.service.ImageHarvestService;
import com.trek.pojo.widen.AssetBatch;
import com.trek.pojo.widen.Item;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Created by mdenomie on 11/9/17.
 */
public class TrekImagePublisher {

    private static Log logger = LogFactory.getLog(TrekImagePublisher.class);

    @Autowired
    ImageHarvestService imageHarvestService;

    protected void run(Date date, Date endDate){
        try {
            AssetBatch assetBatch = imageHarvestService.getInitialAssetBatch(date, endDate);

            logAssets(assetBatch);

            //Keep calling the scroll endpoint until there are no results
            while (assetBatch.getItems() != null && !assetBatch.getItems().isEmpty()) {
                String scrollId = assetBatch.getScrollId();
                assetBatch = imageHarvestService.getNextAssetBatch(scrollId);
                logAssets(assetBatch);
            }
        } catch (Exception e) {
            logger.error("Something went wrong publishing images", e);
        }
    }

    private void logAssets(AssetBatch assetBatch){
        for (Item item : assetBatch.getItems()) {
            logger.info("filename: " + item.getFilename() + "\n" +
                    "last updated:" + item.getLastUpdateDate() + "\n" +
                    "created date: " + item.getCreatedDate() + "\n\n");
        }
    }

    public void setImageHarvestService(ImageHarvestService imageHarvestService) {
        this.imageHarvestService = imageHarvestService;
    }
}
