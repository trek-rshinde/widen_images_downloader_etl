package com.trek.etl.image.exception;

/**
 * Created by mdenomie on 11/14/17.
 */
public class ImageHarvestServiceException extends Exception {
    /**
     *
     */
    public ImageHarvestServiceException() {
    }

    /**
     * @param message
     */
    public ImageHarvestServiceException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public ImageHarvestServiceException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public ImageHarvestServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
