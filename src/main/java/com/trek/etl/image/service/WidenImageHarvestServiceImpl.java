package com.trek.etl.image.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import com.trek.etl.image.exception.ImageHarvestServiceException;
import com.trek.pojo.widen.AssetBatch;
import com.trek.pojo.widen.WidenResultBody;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mdenomie on 11/8/17.
 */
public class WidenImageHarvestServiceImpl implements ImageHarvestService {

    private static Log logger = LogFactory.getLog(WidenImageHarvestServiceImpl.class);
    private static final String MIDNIGHT = "T00:00:00Z";

    private String widenApiBaseUrl;
    private String widenAuthToken;
    private int widenScrollLimit;

    public AssetBatch getInitialAssetBatch(Date beginDate, Date endDate) throws ImageHarvestServiceException {
        WidenResultBody widenResultBody = doInitialSearch(beginDate, endDate);
        return buildAssetBatch(widenResultBody);
    }

    public AssetBatch getNextAssetBatch(String scrollId) throws ImageHarvestServiceException {
        WidenResultBody widenResultBody = doScrollSearch(scrollId);
        return buildAssetBatch(widenResultBody);
    }

    private WidenResultBody doScrollSearch(String scrollId) throws ImageHarvestServiceException{
        logger.info("scrollId: " + scrollId);
        String searchString = "assets/search/scroll?scroll_id=" + scrollId;
        return doSearch(searchString);
    }

    private WidenResultBody doInitialSearch(Date beginDate, Date endDate) throws ImageHarvestServiceException{
        try{
            String dateRangeString = buildDateRangeString(beginDate, endDate);

            String queryString = "scene7:yes (DateAdded:[" + dateRangeString + "] OR LastEditDate:[" + dateRangeString + "])";
            logger.info("queryString: " + queryString);
            String searchString = "assets/search?scroll=true&limit=" + widenScrollLimit + "&query=" + URLEncoder.encode(queryString, "UTF-8");
            return doSearch(searchString);
        } catch (java.io.UnsupportedEncodingException e) {
            logger.error("UnsupportedEncodingException: " + e.getMessage());
            throw new ImageHarvestServiceException(e.getMessage(), e.getCause());
        }
    }

    private String buildDateRangeString(Date beginDate, Date endDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String beginDateString = dateFormat.format(beginDate) + MIDNIGHT;

        // Add a day to the endDate
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();
        String endDateString = dateFormat.format(endDate) + MIDNIGHT;
        return beginDateString + " to " + endDateString;
    }

    private WidenResultBody doSearch(String searchString){
        Client client = ClientBuilder.newClient();

        Response response = client.target(widenApiBaseUrl + searchString)
                .request(MediaType.APPLICATION_JSON).header("Authorization", widenAuthToken).get();

        return response.readEntity(WidenResultBody.class);
    }

    private AssetBatch buildAssetBatch(WidenResultBody widenResultBody){
        AssetBatch assetBatch = new AssetBatch();
        assetBatch.setItems(widenResultBody.getItems());
        assetBatch.setScrollId(widenResultBody.getScrollId());
        return assetBatch;
    }

    public void setWidenApiBaseUrl(String widenApiBaseUrl) {
        this.widenApiBaseUrl = widenApiBaseUrl;
    }

    public void setWidenAuthToken(String widenAuthToken) {
        this.widenAuthToken = widenAuthToken;
    }

    public void setWidenScrollLimit(int widenScrollLimit) {
        this.widenScrollLimit = widenScrollLimit;
    }
}
