package com.trek.etl.image.service;

import com.trek.etl.image.exception.ImageHarvestServiceException;
import com.trek.pojo.widen.AssetBatch;

import java.util.Date;

/**
 * Created by mdenomie on 11/9/17.
 */
public interface ImageHarvestService {

    AssetBatch getInitialAssetBatch(Date beginDate, Date endDate) throws ImageHarvestServiceException;

    AssetBatch getNextAssetBatch(String scrollId) throws ImageHarvestServiceException;

}
