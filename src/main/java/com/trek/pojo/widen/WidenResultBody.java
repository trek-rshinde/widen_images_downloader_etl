
package com.trek.pojo.widen;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "query",
    "sort",
    "query_explained",
    "sort_explained",
    "scroll_id",
    "scroll_timeout",
    "query_syntax_ok",
    "item_type",
    "total_count",
    "offset",
    "limit",
    "items"
})
public class WidenResultBody implements Serializable
{

    @JsonProperty("query")
    private String query;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("query_explained")
    private String queryExplained;
    @JsonProperty("sort_explained")
    private String sortExplained;
    @JsonProperty("scroll_id")
    private String scrollId;
    @JsonProperty("scroll_timeout")
    private Object scrollTimeout;
    @JsonProperty("query_syntax_ok")
    private Boolean querySyntaxOk;
    @JsonProperty("item_type")
    private String itemType;
    @JsonProperty("total_count")
    private Integer totalCount;
    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("limit")
    private Integer limit;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 6677741715216629006L;

    @JsonProperty("query")
    public String getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(String query) {
        this.query = query;
    }

    @JsonProperty("sort")
    public String getSort() {
        return sort;
    }

    @JsonProperty("sort")
    public void setSort(String sort) {
        this.sort = sort;
    }

    @JsonProperty("query_explained")
    public String getQueryExplained() {
        return queryExplained;
    }

    @JsonProperty("query_explained")
    public void setQueryExplained(String queryExplained) {
        this.queryExplained = queryExplained;
    }

    @JsonProperty("sort_explained")
    public String getSortExplained() {
        return sortExplained;
    }

    @JsonProperty("sort_explained")
    public void setSortExplained(String sortExplained) {
        this.sortExplained = sortExplained;
    }

    @JsonProperty("scroll_id")
    public String getScrollId() {
        return scrollId;
    }

    @JsonProperty("scroll_id")
    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }

    @JsonProperty("scroll_timeout")
    public Object getScrollTimeout() {
        return scrollTimeout;
    }

    @JsonProperty("scroll_timeout")
    public void setScrollTimeout(Object scrollTimeout) {
        this.scrollTimeout = scrollTimeout;
    }

    @JsonProperty("query_syntax_ok")
    public Boolean getQuerySyntaxOk() {
        return querySyntaxOk;
    }

    @JsonProperty("query_syntax_ok")
    public void setQuerySyntaxOk(Boolean querySyntaxOk) {
        this.querySyntaxOk = querySyntaxOk;
    }

    @JsonProperty("item_type")
    public String getItemType() {
        return itemType;
    }

    @JsonProperty("item_type")
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    @JsonProperty("total_count")
    public Integer getTotalCount() {
        return totalCount;
    }

    @JsonProperty("total_count")
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @JsonProperty("limit")
    public Integer getLimit() {
        return limit;
    }

    @JsonProperty("limit")
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
