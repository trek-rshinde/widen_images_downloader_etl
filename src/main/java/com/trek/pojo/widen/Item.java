
package com.trek.pojo.widen;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "external_id",
    "filename",
    "created_date",
    "last_update_date",
    "deleted_date",
    "released_and_not_expired",
    "asset_properties",
    "file_properties",
    "metadata",
    "metadata_info",
    "security",
    "thumbnails",
    "embeds",
    "expanded",
    "_links"
})
public class Item implements Serializable
{

    @JsonProperty("id")
    private String id;
    @JsonProperty("external_id")
    private String externalId;
    @JsonProperty("filename")
    private String filename;
    @JsonProperty("created_date")
    private String createdDate;
    @JsonProperty("last_update_date")
    private String lastUpdateDate;
    @JsonProperty("deleted_date")
    private Object deletedDate;
    @JsonProperty("released_and_not_expired")
    private Boolean releasedAndNotExpired;
    @JsonProperty("asset_properties")
    private Object assetProperties;
    @JsonProperty("file_properties")
    private Object fileProperties;
    @JsonProperty("metadata")
    private Object metadata;
    @JsonProperty("metadata_info")
    private Object metadataInfo;
    @JsonProperty("security")
    private Object security;
    @JsonProperty("thumbnails")
    private Object thumbnails;
    @JsonProperty("embeds")
    private Object embeds;
    @JsonProperty("expanded")
    private Expanded expanded;
    @JsonProperty("_links")
    private Links links;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 2127678999543846056L;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("external_id")
    public String getExternalId() {
        return externalId;
    }

    @JsonProperty("external_id")
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @JsonProperty("filename")
    public String getFilename() {
        return filename;
    }

    @JsonProperty("filename")
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @JsonProperty("created_date")
    public String getCreatedDate() {
        return createdDate;
    }

    @JsonProperty("created_date")
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @JsonProperty("last_update_date")
    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    @JsonProperty("last_update_date")
    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @JsonProperty("deleted_date")
    public Object getDeletedDate() {
        return deletedDate;
    }

    @JsonProperty("deleted_date")
    public void setDeletedDate(Object deletedDate) {
        this.deletedDate = deletedDate;
    }

    @JsonProperty("released_and_not_expired")
    public Boolean getReleasedAndNotExpired() {
        return releasedAndNotExpired;
    }

    @JsonProperty("released_and_not_expired")
    public void setReleasedAndNotExpired(Boolean releasedAndNotExpired) {
        this.releasedAndNotExpired = releasedAndNotExpired;
    }

    @JsonProperty("asset_properties")
    public Object getAssetProperties() {
        return assetProperties;
    }

    @JsonProperty("asset_properties")
    public void setAssetProperties(Object assetProperties) {
        this.assetProperties = assetProperties;
    }

    @JsonProperty("file_properties")
    public Object getFileProperties() {
        return fileProperties;
    }

    @JsonProperty("file_properties")
    public void setFileProperties(Object fileProperties) {
        this.fileProperties = fileProperties;
    }

    @JsonProperty("metadata")
    public Object getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    @JsonProperty("metadata_info")
    public Object getMetadataInfo() {
        return metadataInfo;
    }

    @JsonProperty("metadata_info")
    public void setMetadataInfo(Object metadataInfo) {
        this.metadataInfo = metadataInfo;
    }

    @JsonProperty("security")
    public Object getSecurity() {
        return security;
    }

    @JsonProperty("security")
    public void setSecurity(Object security) {
        this.security = security;
    }

    @JsonProperty("thumbnails")
    public Object getThumbnails() {
        return thumbnails;
    }

    @JsonProperty("thumbnails")
    public void setThumbnails(Object thumbnails) {
        this.thumbnails = thumbnails;
    }

    @JsonProperty("embeds")
    public Object getEmbeds() {
        return embeds;
    }

    @JsonProperty("embeds")
    public void setEmbeds(Object embeds) {
        this.embeds = embeds;
    }

    @JsonProperty("expanded")
    public Expanded getExpanded() {
        return expanded;
    }

    @JsonProperty("expanded")
    public void setExpanded(Expanded expanded) {
        this.expanded = expanded;
    }

    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
