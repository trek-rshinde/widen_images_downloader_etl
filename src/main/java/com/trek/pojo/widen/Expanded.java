
package com.trek.pojo.widen;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "asset_properties",
    "embeds",
    "file_properties",
    "metadata",
    "metadata_info",
    "metadata_vocabulary",
    "security",
    "thumbnails"
})
public class Expanded implements Serializable
{

    @JsonProperty("asset_properties")
    private Boolean assetProperties;
    @JsonProperty("embeds")
    private Boolean embeds;
    @JsonProperty("file_properties")
    private Boolean fileProperties;
    @JsonProperty("metadata")
    private Boolean metadata;
    @JsonProperty("metadata_info")
    private Boolean metadataInfo;
    @JsonProperty("metadata_vocabulary")
    private Boolean metadataVocabulary;
    @JsonProperty("security")
    private Boolean security;
    @JsonProperty("thumbnails")
    private Boolean thumbnails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 1491704364595820135L;

    @JsonProperty("asset_properties")
    public Boolean getAssetProperties() {
        return assetProperties;
    }

    @JsonProperty("asset_properties")
    public void setAssetProperties(Boolean assetProperties) {
        this.assetProperties = assetProperties;
    }

    @JsonProperty("embeds")
    public Boolean getEmbeds() {
        return embeds;
    }

    @JsonProperty("embeds")
    public void setEmbeds(Boolean embeds) {
        this.embeds = embeds;
    }

    @JsonProperty("file_properties")
    public Boolean getFileProperties() {
        return fileProperties;
    }

    @JsonProperty("file_properties")
    public void setFileProperties(Boolean fileProperties) {
        this.fileProperties = fileProperties;
    }

    @JsonProperty("metadata")
    public Boolean getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Boolean metadata) {
        this.metadata = metadata;
    }

    @JsonProperty("metadata_info")
    public Boolean getMetadataInfo() {
        return metadataInfo;
    }

    @JsonProperty("metadata_info")
    public void setMetadataInfo(Boolean metadataInfo) {
        this.metadataInfo = metadataInfo;
    }

    @JsonProperty("metadata_vocabulary")
    public Boolean getMetadataVocabulary() {
        return metadataVocabulary;
    }

    @JsonProperty("metadata_vocabulary")
    public void setMetadataVocabulary(Boolean metadataVocabulary) {
        this.metadataVocabulary = metadataVocabulary;
    }

    @JsonProperty("security")
    public Boolean getSecurity() {
        return security;
    }

    @JsonProperty("security")
    public void setSecurity(Boolean security) {
        this.security = security;
    }

    @JsonProperty("thumbnails")
    public Boolean getThumbnails() {
        return thumbnails;
    }

    @JsonProperty("thumbnails")
    public void setThumbnails(Boolean thumbnails) {
        this.thumbnails = thumbnails;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
