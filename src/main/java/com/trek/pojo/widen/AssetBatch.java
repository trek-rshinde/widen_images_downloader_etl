package com.trek.pojo.widen;

import java.util.List;

/**
 * Created by mdenomie on 11/14/17.
 */
public class AssetBatch {
    private List<Item> items;
    private String scrollId;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }
}
