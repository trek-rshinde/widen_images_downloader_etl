#!/bin/bash

ENVIRONMENT=development

LOG=/applogs/widen_to_scene7/$ENVIRONMENT/widen_to_scene7-$(date +%Y%m%d).log

echo Start Time: `date`
echo Start Time: `date` >> $LOG
cd /apps/widen_to_scene7/$ENVIRONMENT
java -Xms256m -Xmx1024m -Dlog4j.configuration=jar:file:jar/tbc-image-publisher-1.0-SNAPSHOT.jar!/log4j.properties -jar jar/tbc-image-publisher-1.0-SNAPSHOT.jar TrekImagePublisherDriver >> $LOG
echo End Time: `date`
echo End Time: `date` >> $LOG
